ztr-backend
=========

A sample app for language learners.

This is only the backend

Install
-------

clone the repo:

      git clone https://bitbucket.org/m3l7/ztr-backend

make sure to have node.js and npm installed, then install the dependencies:

      npm install

make sure to have mongodb properly installed and configured

Config
--------

Just edit server/conf.js


Run
-----

      node server/server.js

Tests
-------

Run the server in test mode:

      NODE_ENV=test node server/server.js

run the tests with npm:

      npm run-script testServer

Under the hood
--------------------

ztr uses the following parts:

* winston for logging and error management
* mocha for testing
* mongodb as database
* passport.js for authentication