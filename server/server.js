var express = require('express')
    , http = require('http')
    , path = require('path')
    , db = require('./db')
    , app = express()
    , passport = require('passport')
    , oauth2 = require('./auth/oauth2')
    , bodyParser = require('body-parser')
    , methodOverride = require('method-override')
    , cookieParser = require('cookie-parser')
    , session = require('cookie-session')
    , conf = require('./conf.js')
    , winstonConf = require('./errorHandling/winstonConf.js')(app) //log/error management (through winston)
    , multer  = require('multer')
    

// all environments
app.set('port', process.env.PORT || 8999);
app.use(bodyParser.json())
app.use(methodOverride());
app.use(multer({ dest: './uploads/'}))


//HTTP LOGGING
if (winstonConf.transports.length) app.use(winstonConf.expressWinston.logger({
      transports: winstonConf.transports,
      meta: false, // optional: control whether you want to log the meta data about the request (default to true)
      msg: "HTTP {{req.method}} {{req.url}} {{res.responseTime}}ms {{res.statusCode}}" // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
}));

app.use(cookieParser());
app.use(session({ secret: 'keyboard cat' }));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, '../app')));


//configure test/production environment
if ('test' == app.get('env')){
    dbConf = conf.db.test
}
else{
    dbConf = conf.db.production
}

require('./auth/passport-config')


//ROUTING
db.connect(dbConf,function(err){
  if (!err){
    var docs = require('./routes/docs')
    , users = require('./routes/users')
    , tags = require('./routes/tags')
    , languages = require('./routes/languages')
    , dict = require('./routes/dicts') 

    //AUTHORIZATION
    app.post('/authorize/token', oauth2.token);
    app.post('/login', oauth2.login);
    app.post('/logout', oauth2.logout);
    app.post('/register',users.register)
    app.post('/activate/:activationCode',users.activate)
    
    //***
    //API
    //***


    app.get('/apiv1/user/:uid',passport.authenticate('bearer', { session: false }),users.info);

    app.get('/apiv1/user/:uid/docs',passport.authenticate('bearer', { session: false }),docs.FindAll); //docs info only
    app.post('/apiv1/user/:uid/docs',passport.authenticate('bearer', { session: false }),docs.New);
    
    app.delete('/apiv1/user/:uid/docs/:docid',passport.authenticate('bearer', { session: false }),docs.Delete);
    app.get('/apiv1/user/:uid/docs/:docid',passport.authenticate('bearer', { session: false }),docs.getdoc); //full doc
    app.get('/apiv1/user/:uid/docs/:docid/info',passport.authenticate('bearer', { session: false }),docs.Metadata);
    app.post('/apiv1/user/:uid/docs/:docid/info',passport.authenticate('bearer', { session: false }),docs.saveMetadata);

    app.post('/apiv1/user/:uid/docs/:docid/newword',passport.authenticate('bearer', { session: false }),docs.savenewword);
    app.delete('/apiv1/user/:uid/docs/:docid/newword',passport.authenticate('bearer', { session: false }),docs.removenewword);
    
    app.delete('/apiv1/user/:uid/tags/:tagname',passport.authenticate('bearer', { session: false }),tags.delete);
    
    app.get('/apiv1/user/:uid/dicts',passport.authenticate('bearer', { session: false }),dict.get_dict_list);
    app.post('/apiv1/user/:uid/dicts',passport.authenticate('bearer', { session: false }),dict.upload_dict);
    app.delete('/apiv1/user/:uid/dicts/:did',passport.authenticate('bearer', { session: false }),dict.delete_dict);



    //error logging/management
    app.use(winstonConf.errorMiddleware)
}})
  


http.createServer(app).listen(app.get('port'), function () {
    console.log("Express server listening on port %d in %s mode", app.get('port'), app.get('env'));
});