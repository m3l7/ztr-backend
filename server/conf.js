var conf = {
	debug: {
		console:{
			enabled: true,
			options:{
				colorize: true,
				level: 'warn' //silly,debug,verbose,info,warn,error
			}
		},
		file:{
			enabled: true,
			options:{
				filename: 'logs.txt',
				level: 'warn' //silly,debug,verbose,info,warn,error
			}
		}
	},
	email:{
                service: 'Gmail',
                user: 'xxxx@gmail.com',
                pass: 'pass'
        },
    site:{
            url: 'http://localhost:8999'
    },
    db:{
    	production:{
            url: "mongodb://localhost:27017/test",
            host: 'localhost',
            dbname: 'test'
    	},
    	test:{
            url: "mongodb://localhost:27017/ztrtest",
            host: 'localhost',
            dbname: 'ztrtest'
    	}
    },
    SALT_WORK_FACTOR : 10
}


module.exports = conf
