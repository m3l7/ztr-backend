var id = require('pow-mongodb-fixtures').createObjectId;
var users = require('./users.js')
var mongo = require('mongodb')

var docs = exports['docs.files'] = {
	activatedUser_doc1 : {
		_id: id(),
	    "filename" : null,
	    "contentType" : "binary/octet-stream",
	    "length" : 12,
	    "chunkSize" : 262144,
	    "uploadDate" : new Date("2014-04-11T00:13:23.868Z"),
	    "aliases" : null,
	    "metadata" : {
	        "user_id" : users.users.activatedUser._id.toString(),
	        "title" : "Clue norsk",
	        "create_date" : new Date("2014-04-11T00:13:23.789Z"),
	        "lang_from" : {
	            "text" : "Italian",
	            "id" : "it"
	        },
	         "lang_to" : {
	            "text" : "French",
	            "id" : "fr"
	        },
	        "tags" : [ 
	            {
	                "text" : "tagtest"
	            },
	            {
	                "text" : "tagtest2"
	            }

        	],
        	"newwords" : [ 
	            {
	                "line" : 5,
	                "fword" : 5,
	                "lword" : 5,
	                "text" : "kandidaten",
	                "origText" : "kandidaten",
	                "transText" : "",
	                "context" : "foran meg fortalte at kandidaten var væpnet med en"
	            }, 
	            {
	                "line" : 5,
	                "fword" : 36,
	                "lword" : 36,
	                "text" : "industribedrift",
	                "origText" : "industribedrift",
	                "transText" : "",
	                "context" : "leder for en norsk industribedrift i klassen mellomstor. "
	            }
        	],
	    },
	    "md5" : "6e45f1a64b8834e15b5122fb50e73a53"
	},

	activatedUser_doc2 : {
	    "filename" : null,
	    "contentType" : "binary/octet-stream",
	    "length" : 9360434,
	    "chunkSize" : 262144,
	    "uploadDate" : new Date("2014-06-11T00:13:23.868Z"),
	    "aliases" : null,
	    "metadata" : {
	        "user_id" : users.users.activatedUser._id.toString(),
	        "title" : "title2",
	        "create_date" : new Date("2014-06-11T00:13:23.789Z"),
	        "tags" : [ 
	            {
	                "text" : "tagtest"
	            },
        	],
	    },
	    "md5" : "6e45f1a64b8834e15b5122fb50e73a53"
	},
	activatedUser2_doc : {
		_id: id(),
	    "filename" : null,
	    "contentType" : "binary/octet-stream",
	    "length" : 12,
	    "chunkSize" : 262144,
	    "uploadDate" : new Date("2014-03-11T00:13:23.868Z"),
	    "aliases" : null,
	    "metadata" : {
	        "user_id" : users.users.activatedUser2._id.toString(),
	        "title" : "ti",
	        "create_date" : new Date("2014-03-11T00:13:23.789Z")
	    },
	    "md5" : "6e45f1a64b8834e15b5122ssfb50e73a53"
	}
}

// var chunks = exports['docs.chunks'] = {
// 	activatedUser2_doca: {
// 	    "files_id" : docs.activatedUser2_doc._id,
// 	    "n" : 0,
// 	    "data" : { "\$y" : "b"}
// 	}
// }


var chunks = exports['docs.chunks'] = {
	activatedUser_doc1: {
	    "files_id" : docs.activatedUser_doc1._id,
	    "n" : 0,
	    "data" : new mongo.Binary("TESTTESTTEST"),
	    'dataClear': 'TESTTESTTEST'
	}
}

