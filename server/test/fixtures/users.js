var id = require('pow-mongodb-fixtures').createObjectId;

var users = exports.users = {
	activatedUser: {
		_id: id(),
	    "username" : "activatedUser",
	    "email" : "pinco@pallino.com",
	    "password" : "$2a$10$fUl1O1hz105q/09l79IeBOdOwZdfFefrnKReyBzWawlikw1SWtNNe", //123456
	    "plainPassword" : "123456", //only for testing
	    "activated" : true,
	    "activationCode" : ""
	},
	activatedUser2: {
		_id: id(),
	    "username" : "activatedUser2",
	    "email" : "lol@copter.com",
	    "password" : "$2a$10$fUl1O1hz105q/09l79IeBOdOwZdfFefrnKReyBzWawlikw1SWtNNe", //123456
	    "plainPassword" : "123456", //only for testing
	    "activated" : true,
	    "activationCode" : ""
	},
	inactivatedUser: {
		_id: id(),
	    "username" : "inactivatedUser",
	    "email" : "mario@rossi.it",
	    "password" : "$2a$10$5qtoaGP28dpLvccnCHpRH.BCMKU3nAyQlWdYDoE3vftmQSxkJx8le", //12345
	    "password" : "12345", //only for testing
	    "activated" : false,
	    "activationCode" : "ICgPJ9_Wc8oJPcPfsLHViqqcxcRznXBHfoZlUsI-Jl9BMnQfib9IayRB2ZW28S_8"
	}
}

var d = new Date()
d.setMonth(d.getMonth() + 1);

exports.tokens = {
	activatedUser: {
	    "user_id" : users.activatedUser._id,
	    "token" : "cHWVoIqa52jp90z3",
	    "expiry_date" : d,
	}
}