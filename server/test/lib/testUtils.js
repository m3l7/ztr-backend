exports.docObjectEqual = function(obj1,obj2){
	if ((!obj1) || (!obj1.metadata)) return false
	if ((!obj2) || (!obj2.metadata)) return false

	var metadata = obj1.metadata
	var _metadata = obj2.metadata
	if (obj1.length != (obj2.length)) return false
	if (obj1.chunkSize != (obj2.chunkSize)) return false
	if (new Date(obj1.uploadDate).toString() != (obj2.uploadDate.toString()) ) return false
	if (metadata.user_id != (_metadata.user_id) ) return false
	if (metadata.title != (_metadata.title) ) return false
	if (new Date(metadata.create_date).toString() != (_metadata.create_date.toString()) ) return false
	if (!!metadata.lang_from){
		if (metadata.lang_from.text != (_metadata.lang_from.text)) return false
		if (metadata.lang_from.id != (_metadata.lang_from.id)) return false
	}
	if (!!metadata.lang_to){
		if (metadata.lang_to.text != (_metadata.lang_to.text)) return false
		if (metadata.lang_to.id != (_metadata.lang_to.id)) return false
	}
	if (!metadata.tags) return false
	if (!_metadata.tags) return false
	if (metadata.tags.length != (_metadata.tags.length)) return false
	if (metadata.tags.length)
		if (metadata.tags[0].text != (_metadata.tags[0].text)) return false

	var neww = metadata.newwords
	var _neww = _metadata.newwords
	
	if ((!!neww) && (neww.length)){
		if (neww.length != (_neww.length)) return false
		if (neww[0].line != (_neww[0].line)) return false
		if (neww[0].fword != (_neww[0].fword)) return false
		if (neww[0].lword != (_neww[0].lword)) return false
		if (neww[0].text != (_neww[0].text)) return false
		if (neww[0].origText != (_neww[0].origText)) return false
		if (neww[0].transText != (_neww[0].transText)) return false
		if (neww[0].context != (_neww[0].context)) return false
	}
	return true
}