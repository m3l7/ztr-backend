//END-TO-END TESTS FOR APIV1

var should = require('should');
var shouldhttp = require('should-http');
var assert = require('assert');
var request = require('supertest');
var db = require('../db')
var conf = require('../conf.js')
var users = require('./fixtures/users.js')
var docs = require('./fixtures/docs.js')
var testUtils = require('./lib/testUtils.js')

var agent = request.agent(conf.site.url)



before(function(done){
	db.connect(conf.db.test,function(){
		done()
	})
})

after(function(done){
	var fixtures = require('pow-mongodb-fixtures').connect(conf.db.test.dbname, conf.db.test)
	fixtures.clear(function(){ done() })
})

beforeEach(function(done){
	//load fixtures into the test db
	var fixtures = require('pow-mongodb-fixtures').connect(conf.db.test.dbname, conf.db.test)

	fixtures.clearAllAndLoad(__dirname + '/fixtures/users.js',function(){
		fixtures.load(__dirname + '/fixtures/docs.js',function(){ done() })
	})

})


describe('Authentication',function(){


	beforeEach(function(done){
		//load fixtures into the test db
		var fixtures = require('pow-mongodb-fixtures').connect(conf.db.test.dbname, conf.db.test)
		fixtures.clear(function(){
			fixtures.load(__dirname + '/fixtures/users.js',function(){
				done()
			})
		})
	})

	describe('Registration',function(){

		it('should register a new user',function(done){
			var user = {
			    "user" : "newUser",
			    "email" : "new@userztr.com",
			    "pass" : "123456",
			}
			request(conf.site.url).post('/register').send(user)
			.end(function(err,res){
				should.not.exist(err)
				res.should.have.status(200)
				done()
			})
		})

		it('should fail when register a duplicate user',function(done){
			var user = {
			    "user" : users.users.inactivatedUser.username,
			    "email" : users.users.inactivatedUser.email,
			    "pass" : "1111111"
			}
			request(conf.site.url).post('/register').send(user)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})

		it('should fail when register an invalid email',function(done){
			var user = {
			    "user" : "test",
			    "email" : "mario",
			    "pass" : "1111111", //12345
			}
			request(conf.site.url).post('/register').send(user)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})

		it('should fail when register an invalid (short) pass',function(done){
			var user = {
			    "user" : "test2",
			    "email" : "short@passw.com",
			    "pass" : "11", //12345
			}
			request(conf.site.url).post('/register').send(user)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})

	})

	describe('Activation',function(){

		it('Should activate a non activated user with a valid code',function(done){
			var code = users.users.inactivatedUser.activationCode
			request(conf.site.url).post('/activate/'+code).send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				done()
			})
		})
		it('Should not activate with an empty code',function(done){
			request(conf.site.url).post('/activate/').send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(404)
				done()
			})
		})
		it('Should not activate with an invalid code',function(done){
			request(conf.site.url).post('/activate/111aaa').send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})
	})

	describe('Login',function(){
		it('Should login with a valid user/pass for an activated user.',function(done){
			var username = users.users.activatedUser.username
			var password = users.users.activatedUser.plainPassword
			request(conf.site.url).post('/login?username='+username+'&password='+password).send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				done()
			})
		})
		it('Should not login with a invalid user/pass for an activated user.',function(done){
			request(conf.site.url).post('/login?username=activatedUser&password=fail').send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(401)
				done()
			})
		})
		it('Should not login with a valid user/pass for an inactivated user.',function(done){
			var username = users.users.inactivatedUser.username
			var password = users.users.inactivatedUser.plainPassword
			request(conf.site.url).post('/login?username='+username+'&password='+password).send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(401)
				done()
			})
		})
		

	})

	describe('Logout',function(){
		it('Should Logout',function(done){
			var username = users.users.activatedUser.username
			var password = users.users.activatedUser.plainPassword
			var agent = request.agent(conf.site.url)
			agent.post('/login?username='+username+'&password='+password)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				agent.saveCookies(res)

				agent.post('/Logout')
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					done()
				})
			
			})
		})
	})

	describe('Token Grant',function(){
		it('Should get an access token and the uid when logged in',function(done){
			var username = users.users.activatedUser.username
			var password = users.users.activatedUser.plainPassword
			var agent = request.agent(conf.site.url)
			agent.post('/login?username='+username+'&password='+password)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				agent.saveCookies(res)
				
				agent.post('/authorize/token')
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					res.text.should.be.ok
					JSON.parse(res.text).token.should.not.be.empty
					JSON.parse(res.text).uid.should.not.be.empty
					done()
				})
			})
		})

		it('Should not get an access token if not logged in',function(done){
			request(conf.site.url).post('/authorize/token').send()
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(302)
				done()
			})
		})

		it('Should not get an access token after logout',function(done){
			var username = users.users.activatedUser.username
			var password = users.users.activatedUser.plainPassword
			var agent = request.agent(conf.site.url)
			agent.post('/login?username='+username+'&password='+password)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				agent.saveCookies(res)

				agent.post('/Logout')
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					agent.saveCookies(res)
					
					agent.post('/authorize/token')
					.end(function(err,res){
						should.not.exist(err)
						res.status.should.equal(302)
						done()
					})
				})
			
			})
		})
	

	})


})



describe('Users',function(){

	beforeEach(function(done){
		//load fixtures into the test db
		var fixtures = require('pow-mongodb-fixtures').connect(conf.db.test.dbname, conf.db.test)
		fixtures.clear(function(){
			fixtures.load(__dirname + '/fixtures/users.js',function(){
				done()
			})
		})
	})

	describe('Permission Restrictions',function(){
		it('Should not access others user info',function(done){
			agent.get('/apiv1/user/'+users.users.activatedUser2._id)
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(401)
				done()
			})
		})
	})

	describe('User info',function(){
		it('Should get own user info',function(done){
			agent.get('/apiv1/user/'+users.users.activatedUser._id)
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				res.text.should.be.ok
				var text = JSON.parse(res.text)[0]

				text._id.should.equal(users.users.activatedUser._id.toString())
				text.username.should.equal(users.users.activatedUser.username)
				text.email.should.equal(users.users.activatedUser.email)
				text.plainPassword.should.equal(users.users.activatedUser.plainPassword)
				text.activated.should.equal(users.users.activatedUser.activated)
				text.activationCode.should.equal(users.users.activatedUser.activationCode)
				
				done()
			})
		})
	})
})

describe('Docs',function(){

	beforeEach(function(done){
		//load fixtures into the test db
		var fixtures = require('pow-mongodb-fixtures').connect(conf.db.test.dbname, conf.db.test)

		fixtures.clearAllAndLoad(__dirname + '/fixtures/users.js',function(){
			fixtures.load(__dirname + '/fixtures/docs.js',function(){ done() })
		})
	})

	describe('Permission Restrictions',function(){
		it('Should not access others docs',function(done){
			var uid = users.users.activatedUser2._id
			agent.get('/apiv1/user/'+uid+'/docs')
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(401)
				res.text.should.be.ok
				done()
			})
		})

	})

	describe('Docs',function(){
		it('Should get own docs',function(done){
			var uid = users.users.activatedUser._id
			agent.get('/apiv1/user/'+uid+'/docs')
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				res.text.should.be.ok
				var text = JSON.parse(res.text)
				text.should.have.lengthOf(2)

				testUtils.docObjectEqual(text[0],docs['docs.files'].activatedUser_doc1).should.be.true

				done()
			})
		})

		it('Should Create a new Doc',function(done){
			var uid = users.users.activatedUser._id
			agent.post('/apiv1/user/'+uid+'/docs?name=testfile')
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.attach('file',__dirname + '/fixtures/testFile.txt','testFile.txt')
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				done()
			})
		})

		it('Should Create a new Doc with the same name of a doc of another user',function(done){
			var uid = users.users.activatedUser._id
			agent.post('/apiv1/user/'+uid+'/docs?name='+docs['docs.files'].activatedUser2_doc.metadata.title)
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.attach('file',__dirname + '/fixtures/testFile.txt','testFile.txt')
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				done()
			})
		})


		it('Should not create a duplicate Doc',function(done){
			var uid = users.users.activatedUser._id
			agent.post('/apiv1/user/'+uid+'/docs?name='+docs['docs.files'].activatedUser_doc1.metadata.title)
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.attach('file',__dirname + '/fixtures/testFile.txt','testFile.txt')
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})

		it('Should not create a doc with no file uploaded',function(done){
			var uid = users.users.activatedUser._id
			agent.post('/apiv1/user/'+uid+'/docs?name='+docs['docs.files'].activatedUser_doc1.metadata.title)
			.set('Authorization','Bearer '+users.tokens.activatedUser.token)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})

	})

	
})



describe('Doc',function(){

	var uid1 = users.users.activatedUser._id
	var uid2 = users.users.activatedUser2._id
	var doc2 = docs['docs.files'].activatedUser2_doc._id
	var doc1 = docs['docs.files'].activatedUser_doc1._id
	var token1 = users.tokens.activatedUser.token
	
	describe('Permission Restrictions',function(){

		it('Should not get a doc of a different user',function(done){
			agent.get('/apiv1/user/'+uid1+'/docs/'+doc2)
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				agent.get('/apiv1/user/'+uid2+'/docs/'+doc2)
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(401)
					done()
				})
				
			})

		})

		it('Should not delete a doc of a different user',function(done){
			agent.delete('/apiv1/user/'+uid1+'/docs/'+doc2)
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				agent.delete('/apiv1/user/'+uid2+'/docs/'+doc2)
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(401)
					done()
				})
				
			})

		})

	})

	describe('Doc',function(){

			it('Should get an own file',function(done){
				agent.get('/apiv1/user/'+uid1+'/docs/'+doc1)
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					res.text.should.equal(docs['docs.chunks'].activatedUser_doc1.dataClear)
					done()
				})
				
			})

			it('Should delete an existent doc',function(done){
				agent.delete('/apiv1/user/'+uid1+'/docs/'+doc1)
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					done()
				})
			})

			it('Should not delete a non existent doc',function(done){
				agent.delete('/apiv1/user/'+uid1+'/docs/123456789012')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(400)
					done()
				})
			})

			it('Should not get a non existent doc',function(done){
				agent.get('/apiv1/user/'+uid1+'/docs/123456789012')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(400)
					done()
				})
			})

	})

})


describe('DocInfo',function(){

	var uid1 = users.users.activatedUser._id
	var uid2 = users.users.activatedUser2._id
	var doc2 = docs['docs.files'].activatedUser2_doc._id
	var doc1 = docs['docs.files'].activatedUser_doc1._id
	var token1 = users.tokens.activatedUser.token

	describe('Permission Restrictions',function(){

		it('Should not get doc info of a different user',function(done){
			agent.get('/apiv1/user/'+uid1+'/docs/'+doc2+'/info')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				agent.get('/apiv1/user/'+uid2+'/docs/'+doc2+'/info')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(401)
					done()
				})
				
			})

		})

		it('Should not post a doc info of a different user',function(done){
			agent.post('/apiv1/user/'+uid1+'/docs/'+doc2+'/info')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				agent.post('/apiv1/user/'+uid2+'/docs/'+doc2+'/info')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(401)
					done()
				})
				
			})

		})

	})

	describe('DocInfo',function(){

		it('Should get doc info',function(done){
			agent.get('/apiv1/user/'+uid1+'/docs/'+doc1+'/info')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				JSON.parse(res.text).should.be.ok
				JSON.parse(res.text).should.have.lengthOf(1)
				var docObj = JSON.parse(res.text)[0]

				testUtils.docObjectEqual(docObj,docs['docs.files'].activatedUser_doc1).should.be.true

				done()
			})

		})

		it('Should update doc info',function(done){
			var docObj1 = docs['docs.files'].activatedUser_doc1
			
			docObj1.metadata.tags.push({'text': 'newtag'})
			docObj1.metadata.lang_from.text = 'Esperanto'
			docObj1.metadata.title = 'newtitle'

			agent.post('/apiv1/user/'+uid1+'/docs/'+doc1+'/info')
			.set('Authorization','Bearer '+token1)
			.send(docObj1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)

				agent.get('/apiv1/user/'+uid1+'/docs/'+doc1+'/info')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					JSON.parse(res.text).should.be.ok
					JSON.parse(res.text).should.have.lengthOf(1)
					var docObj = JSON.parse(res.text)[0]

					testUtils.docObjectEqual(docObj,docObj1).should.be.true

					done()
				})
			})

		})

		it('Should not get doc info of a non existent doc',function(done){
			agent.get('/apiv1/user/'+uid1+'/docs/'+'123456789012'+'/info')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})

		})

		it('Should not update doc info of a non existent doc',function(done){
			var docObj1 = docs['docs.files'].activatedUser_doc1
			
			docObj1.metadata.tags.push({'text': 'newtag'})
			docObj1.metadata.lang_from.text = 'Esperanto'
			docObj1.metadata.title = 'newtitle'

			agent.post('/apiv1/user/'+uid1+'/docs/'+'123456789012'+'/info')
			.set('Authorization','Bearer '+token1)
			.send(docObj1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})

		})
	})

})

describe('New Words',function(){

	var uid1 = users.users.activatedUser._id
	var uid2 = users.users.activatedUser2._id
	var doc2 = docs['docs.files'].activatedUser2_doc._id
	var doc1 = docs['docs.files'].activatedUser_doc1._id
	var token1 = users.tokens.activatedUser.token

	describe('Permission Restrictions',function(){

		it('Should not remove a word of a different user',function(done){
			agent.delete('/apiv1/user/'+uid1+'/docs/'+doc2+'/newword?fword=5&lword=5&line=5')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				agent.delete('/apiv1/user/'+uid2+'/docs/'+doc2+'/newword?fword=5&lword=5&line=5')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(401)
					done()
				})
				
			})

		})

		it('Should not post a word of a different user',function(done){
			var newword = {"line":7,"text":"plassert","fword":6,"lword":6,"context":"vannglasset sekretren min hadde plassert p det lave bordet","transText":"","origText":"plassert","update":false}
			agent.post('/apiv1/user/'+uid1+'/docs/'+doc2+'/newword?fword=5&lword=5&line=5')
			.set('Authorization','Bearer '+token1)
			.send(newword)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				agent.post('/apiv1/user/'+uid2+'/docs/'+doc2+'/newword')
				.set('Authorization','Bearer '+token1)
				.send(newword)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(401)
					done()
				})
				
			})

		})

	})

	describe('New Words',function(){
		it('Should remove a word',function(done){
			var docObj1 = docs['docs.files'].activatedUser_doc1
			docObj1.metadata.newwords.splice(0,1)
			agent.delete('/apiv1/user/'+uid1+'/docs/'+doc1+'/newword?fword=5&lword=5&line=5')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)

				agent.get('/apiv1/user/'+uid1+'/docs/'+doc1+'/info')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					
					res.text.should.be.ok
					JSON.parse(res.text).should.have.lengthOf(1)
					var docObj = JSON.parse(res.text)[0]
					docObj.metadata.should.be.ok

					testUtils.docObjectEqual(docObj,docObj1).should.be.true

					done()

				})
				
			})

		})

		it('Should not remove a non existent word',function(done){
			agent.delete('/apiv1/user/'+uid1+'/docs/'+doc1+'/newword?fword=500&lword=5111&line=5222')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)
				done()
			})
		})

		it('Should create a new word',function(done){
			var newword = {"line":7,"text":"plassert","fword":6,"lword":6,"context":"vannglasset sekretren min hadde plassert p det lave bordet","transText":"","origText":"plassert","update":false}
			var docObj1 = docs['docs.files'].activatedUser_doc1
			docObj1.metadata.newwords.push(newword)
			agent.post('/apiv1/user/'+uid1+'/docs/'+doc1+'/newword')
			.set('Authorization','Bearer '+token1)
			.send(newword)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)
				agent.get('/apiv1/user/'+uid1+'/docs/'+doc1+'/info')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					
					res.text.should.be.ok
					JSON.parse(res.text).should.have.lengthOf(1)
					var docObj = JSON.parse(res.text)[0]
					docObj.metadata.should.be.ok

					testUtils.docObjectEqual(docObj,docObj1).should.be.true

					done()

				})
			})
		})


	})

})


describe('Tags',function(){

	var uid1 = users.users.activatedUser._id
	var uid2 = users.users.activatedUser2._id
	var doc2 = docs['docs.files'].activatedUser2_doc._id
	var doc1 = docs['docs.files'].activatedUser_doc1._id
	var token1 = users.tokens.activatedUser.token
	var docObj1 = docs['docs.files'].activatedUser_doc1
	var docObj2 = docs['docs.files'].activatedUser_doc2

	describe('Permission Restrictions',function(){

		it('Should not delete a tag of a different user',function(done){
			agent.delete('/apiv1/user/'+uid2+'/tags/'+docObj1.metadata.tags[0].text)
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(401)
				done()
			})

		})

	})

	describe('Tags',function(){
		it('Should delete a tag',function(done){
			agent.delete('/apiv1/user/'+uid1+'/tags/'+docObj1.metadata.tags[0].text)
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(200)

				agent.get('/apiv1/user/'+uid1+'/docs/')
				.set('Authorization','Bearer '+token1)
				.end(function(err,res){
					should.not.exist(err)
					res.status.should.equal(200)
					
					res.text.should.be.ok
					JSON.parse(res.text).should.have.lengthOf(2)
					var docObjRes1 = JSON.parse(res.text)[0]
					var docObjRes2 = JSON.parse(res.text)[1]
					docObj1.metadata.tags.splice(0,1)
					docObj2.metadata.tags.splice(0,1)

					testUtils.docObjectEqual(docObjRes1,docObj1).should.be.true
					testUtils.docObjectEqual(docObjRes2,docObj2).should.be.true

					done()
				})
			})

		})

		it('Should not delete a non existent tag',function(done){
			agent.delete('/apiv1/user/'+uid1+'/tags/wewewewewe')
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(400)

				done()

			})

		})
	})

})


describe.skip('Dictionaries',function(){

	var uid1 = users.users.activatedUser._id
	var uid2 = users.users.activatedUser2._id
	var doc2 = docs['docs.files'].activatedUser2_doc._id
	var doc1 = docs['docs.files'].activatedUser_doc1._id
	var token1 = users.tokens.activatedUser.token
	var docObj1 = docs['docs.files'].activatedUser_doc1
	var docObj2 = docs['docs.files'].activatedUser_doc2

	describe('Permission Restrictions',function(){

		it('Should not delete a tag of a different user',function(done){
			agent.delete('/apiv1/user/'+uid2+'/tags/'+docObj1.metadata.tags[0].text)
			.set('Authorization','Bearer '+token1)
			.end(function(err,res){
				should.not.exist(err)
				res.status.should.equal(401)
				done()
			})

		})

	})

})