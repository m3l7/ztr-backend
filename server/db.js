var MongoClient = require('mongodb').MongoClient
, mongo = require('mongodb')
, Grid = require('mongodb').Grid
, BSON = require('mongodb').BSONPure
, Server = require('mongodb').Server

var my = {
  db: null,
  collection: function(name) {
    if (my.db == null)
      throw new Error('Not connected to database');
    else
      return this.db[name];
  },
  connect: function(dbConf,callback) {
    if (my.db!=null) {
      console.log("already connected to the db!")
      callback(null,my.db)
    }
    else MongoClient.connect(dbConf.url, {
      server: {
        autoreconnect: true,
        poolSize: 10
      }
    }, function(err, db) {
      // save reference to db
      if (err) console.log("db_err "+err)
      else {
      	my.db = db;
      }
      callback(err, db);
    });
  }
  
}
module.exports = my
  