var dbmod = require('../db.js')
var BSON = require('mongodb').BSONPure
, ObjectID = require('mongodb').ObjectID
, winstonConf = require('../errorHandling/winstonConf.js')() //log/error management (through winston)

var db = dbmod.db
var docs = db.collection('docs.files')

exports.delete = function(req,res,next){ 
  
  if (req.params.uid==req.user._id){
    var name = req.params.tagname
    docs.update({'metadata.user_id':req.user._id.toString(),'metadata.tags.text': name},
		{$pull: {'metadata.tags': {'text':name}}},{multi: true},function(err,WriteResult){
      if (!!err) next(new Error(err))
      else if (WriteResult==0) winstonConf.handleErrorCode(400,'Invalid Tag',next)
      else res.send()
    })
  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}
