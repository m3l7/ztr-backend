var dbmod = require('../db')
, BSON = require('mongodb').BSONPure
, Grid = require('mongodb').Grid
, fs = require('fs')
, ObjectId = require('mongodb').ObjectID

var db = dbmod.db



exports.get_dict_list = function(req,res){
  var db = dbmod.db
  if (req.params.uid==req.user._id){
  	dicts = db.collection('dicts.files')
  	dicts.find({'metadata.user_id': req.user._id.toString()}).toArray(function(err,items){
  		if (!err) res.send(items)
  		else res.send(500) 
  	})
  }
  else res.send(401)
}
exports.upload_dict = function(req,res){
  var db = dbmod.db
  if ((req.files.file.path!=undefined) && (req.files.file.path!='') && (req.params.uid==req.user._id)){
        collection = db.collection('dicts.files')
        var filename = ''
        if (req.query.name!=undefined) {filename = req.query.name}
        else {filename =  req.files.file.name}
        
        fs.readFile(req.files.file.path, function (err, buff){
              buff=new Buffer(buff.toString("binary").replace(/\n+/g,"\n"),"binary")
              if (err) console.log(err)
              var grid = new Grid(db, 'dicts');
              grid.put(
                buff, 
                {metadata:{
                  user_id:req.user._id.toString(),
                  title: filename,
                  create_date: new Date(),
                }}, function(err, fileInfo) {
                  if(!err) res.send()
                  else console.log(err)
                }
              )
        })
  }
  else res.send(401)
}


exports.delete_dict = function(req,res){
  var db = dbmod.db
  if (req.params.uid==req.user._id){
    dicts = db.collection('dicts.files')
    var chunks = db.collection('dicts.chunks')

    dicts.remove({'metadata.user_id': req.user._id.toString(),'_id': BSON.ObjectID(req.params.did)},function(err){
      //WARNING: check correctly the error!!!!
      if (!err) chunks.remove({'files_id': BSON.ObjectID(req.params.did)},function(err){
         if (err) res.send(500)
         else res.send()
      })
      else res.send(500)
    })
  }
  else res.send(401)
}