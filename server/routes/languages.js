var dbmod = require('../db.js')
var BSON = require('mongodb').BSONPure

exports.FindAll = function(req,res){
 var db = dbmod.db
 if (req.params.uid==req.user._id){
	 collection = db.collection('languages')
	 collection.find({'user_id': req.user._id}).toArray(function(err, items) {
	            res.send(items);
	 });
 }
 else res.send(304)
}