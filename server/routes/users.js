var dbmod = require('../db')
, BSON = require('mongodb').BSONPure
, Grid = require('mongodb').Grid
, fs = require('fs')
, ObjectId = require('mongodb').ObjectID
, bcrypt = require('bcrypt')
, nodemailer = require('nodemailer')
, crypto = require('crypto')
, conf = require('../conf.js')
, winstonConf = require('../errorHandling/winstonConf.js')() //log/error management (through winston)


function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

exports.info = function(req,res,next){
  if (req.params.uid==req.user._id) res.send("["+JSON.stringify(req.user)+"]")
  else winstonConf.handleErrorCode(401,'Permission denied',next)
}

exports.register = function(req,res,next){
	var db = dbmod.db
	var user = req.body.user
		,pass = req.body.pass
		,email = req.body.email

	if ((!user) || (!pass) || (!email)) winstonConf.handleErrorCode(400,'Invalid register parameters',next)
	else if (!validateEmail(email)) winstonConf.handleErrorCode(400,'Invalid email',next)
	else if (pass.length<5) winstonConf.handleErrorCode(400,'Password must be at least 5 characters long',next)
	else{
		var users = db.collection("users")
		users.findOne({$or:[{'username':user},{'email':email}]},function(err,item){
			if (!!err) next(new Error(err))
			else if (!!item) {
				if (item.email==email) winstonConf.handleErrorCode(400,'Email already exists. Please choose a different one',next)
				else winstonConf.handleErrorCode(400,'User already exists. Please choose a different one',next)
			}
			else{
				//generate the pass salt+hash
				bcrypt.hash(pass, conf.SALT_WORK_FACTOR, function(err, hash) {
					if (!!err) next(new Error(err))
					else {
						//generate activation code
						crypto.randomBytes(48, function(ex, buf) {
							activationToken = buf.toString('base64').replace(/\//g,'_').replace(/\+/g,'-')
							users.insert({
								username: user,
								email: email,	
								password: hash,
								activated: false,
								activationCode: activationToken
							},function(err){
								if (!!err) next(new Error(err))
								else {
									res.send()
									sendActivationMail(email,activationToken)
								}
							})

						});
					}
				});
			}
		})
	}
}


exports.activate = function(req,res,next){
	if (!req.params.activationCode) winstonConf.handleErrorCode(400,'Invalid activation code',next)
	else{
		var db = dbmod.db
		var users = db.collection('users')
		users.findOne({activationCode: req.params.activationCode},function(err,item){
			if (!!err) next(new Error(err))
			else if (!item) winstonConf.handleErrorCode(400,'Invalid activation code',next)
			else {
				item.activationCode = null
				item.activated = true
				users.update({'username':item.username},
              		{$set:{'activationCode':'',activated:true}},function(err){
              			if (!!err) next(new Error(err))
              			else res.send()
              	})
			}
		})
	}
}

var sendActivationMail = function(email,activationCode){
	var activationLink = conf.site.url+"/#/activate/"+activationCode

	// create reusable transport method (opens pool of SMTP connections)
	var smtpTransport = nodemailer.createTransport("SMTP",{
	    service: conf.email.service,
	    auth: {
	        user: conf.email.user,
	        pass: conf.email.pass
	    }
	});

	// setup e-mail data with unicode symbols
	var emailText = "<p>Thank you for registering.<p>Please click the link below to activate your account.</p><a href='"+activationLink+"'>"+activationLink+"</a><p>Regards.</p><p>Filippo Zonta.</p>"

	var mailOptions = {
	    from: "ZTR ✔ <noreply@ztr.com>", // sender address
	    to: email, // list of receivers
	    subject: "ZTR Activation Code", // Subject line
	    html: emailText
	}

	// send mail with defined transport object
	smtpTransport.sendMail(mailOptions, function(error, response){
	    if(error){
	        console.log(error);
	    }else{
	        console.log("Message sent: " + response.message);
	    }
	});
}