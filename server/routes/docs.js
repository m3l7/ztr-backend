var dbmod = require('../db')
, BSON = require('mongodb').BSONPure
, Grid = require('mongodb').Grid
, fs = require('fs')
, ObjectId = require('mongodb').ObjectID
, winstonConf = require('../errorHandling/winstonConf.js')() //log/error management (through winston)


var db = dbmod.db
var docs = db.collection('docs.files')
var chunks = db.collection('docs.chunks')

exports.FindAll = function(req,res,next){ 
  if (req.params.uid==req.user._id){
    docs.find({'metadata.user_id': req.user._id.toString()}).toArray(function(err, items) {
        if (!!err) next(new Error(err))
        else res.send(items);
    });
  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}

exports.saveMetadata = function(req,res,next){ 
  if (req.params.uid==req.user._id){
    docs.update({'metadata.user_id': req.user._id.toString(),'_id':BSON.ObjectID(req.params.docid)},
		{$set: {'metadata': req.body.metadata}},function(err,WriteResult){
    	if (!!err) next(new Error(err))
      else if (WriteResult==0) winstonConf.handleErrorCode(400,'Invalid docid',next)
      else res.send()
    })
  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}

exports.Metadata = function(req,res,next){ 
  if (req.params.uid==req.user._id){
    docs.find({'metadata.user_id': req.user._id.toString(),'_id':BSON.ObjectID(req.params.docid)}).toArray(function(err, items) {
      if (!!err) next(new Error(err))
      else if (!items.length) winstonConf.handleErrorCode(400,'Invalid docid',next)
      else res.send([items[0]])
    })
  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}

exports.Delete = function(req,res,next){ 
  if (req.params.uid==req.user._id){
    docs.remove({'metadata.user_id': req.user._id.toString(),'_id': BSON.ObjectID(req.params.docid)},function(err,WriteResult){
      if (!!err) next(new Error(err))
      else if (WriteResult==0) winstonConf.handleErrorCode(400,'Invalid docid',next)
      else chunks.remove({'files_id': BSON.ObjectID(req.params.docid)},function(err){
	       if (!!err) next(new Error(err))
	       else res.send()
      })
    })
  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}

exports.New = function(req,res,next){ 
  if (req.params.uid!=req.user._id) winstonConf.handleErrorCode(401,'Permission Denied',next)
  else if ((!!req.files) && (!!req.files.file) && (!!req.files.file.path)){
    var filename = ''
    if (!!req.query.name) {filename = req.query.name}
    else {filename =  req.files.file.name}
    
    docs.findOne({'metadata.user_id': req.user._id.toString(),'metadata.title': filename},function(err,item){
      if (!!err) next(new Error(err))
      else if (!!item) winstonConf.handleErrorCode(400,'Duplicate Doc',next)
      else{

        fs.readFile(req.files.file.path, function (err, buff){
          buff=new Buffer(buff.toString("binary").replace(/\n+/g,"\n"),"binary")
          if (err) console.log(err)
          var grid = new Grid(db, 'docs');
          grid.put(buff, 
              {metadata:{
                user_id:req.user._id.toString(),
                title: filename,
                create_date: new Date(),
                lang_from: {
                  id: '',
                  text: ''
                },
                lang_to: '',
                bookmarks: [],
                tags: [],
                dictionaries: [],
                newwords:[]
              }}, function(err, fileInfo) {
                if(!err) res.send()
                else next(new Error(err))
              }
          )
        })

      }
    })

  }
  else winstonConf.handleErrorCode(400,'File not provided',next)
}


exports.getdoc = function(req,res,next){ 
  if (req.params.uid==req.user._id){
    var grid = new Grid(db, 'docs');
    docs.findOne({'_id': ObjectId(req.params.docid),'metadata.user_id': req.params.uid},function(err,item){
      if (!!err) next(new Error(err))
      else if (!item) winstonConf.handleErrorCode(400,'Invalid docid',next)
      else grid.get(ObjectId(req.params.docid),function(err,data){
        if (!!err) next(new Error(err))
        else res.send(data)
      })
    })
  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}
exports.savenewword = function(req,res,next){
  if (req.params.uid==req.user._id){
    //TODO: CHECK IF THE NEW WORD IS ALREADY INSERTED!!!!!!!!!!!
    docs.findOne({'metadata.user_id': req.user._id.toString(),'_id':BSON.ObjectID(req.params.docid)},function(err,item){
      if (!!err) next(new Error(err))
      else if (!item) winstonConf.handleErrorCode(400,'Invalid docid',next)
      else{
          var updated = false
          if (!req.body.update){
            updated = true
            item.metadata.newwords.push({
              line: req.body.line,
              fword: req.body.fword,
              lword: req.body.lword,
              text: req.body.text,
              origText: req.body.origText,
              transText: req.body.transText,
              context: req.body.context
            })
          }
          else{
            var nw = item.metadata.newwords
            for (var i = 0; i < nw.length; i++) {
              if ((req.body.line==nw[i].line) && (req.body.fword==nw[i].fword)){
                updated = true
                item.metadata.newwords[i].line = req.body.line
                item.metadata.newwords[i].fword = req.body.fword
                item.metadata.newwords[i].lword = req.body.lword
                item.metadata.newwords[i].text = req.body.text
                item.metadata.newwords[i].origText = req.body.origText
                item.metadata.newwords[i].transText = req.body.transText
                item.metadata.newwords[i].context = req.body.context
              }
            }
          }
          if (!updated) winstonConf.handleErrorCode(400,'Invalid data',next)
          else docs.update({'_id':BSON.ObjectID(req.params.docid)},
                  {$set:{'metadata.newwords':item.metadata.newwords}},function(err,WriteResult){
                    if (!!err) next(new Error(err))
                    else if (WriteResult==0) next(new Error('Can\'t post word'))
                    else res.send()
                  })
      }
    })

  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}
exports.removenewword = function(req,res,next){
  if (req.params.uid==req.user._id){
    //TODO: CHECK IF THE NEW WORD IS ALREADY INSERTED!!!!!!!!!!!
    docs.findOne({'metadata.user_id': req.user._id.toString(),'_id':BSON.ObjectID(req.params.docid)},function(err,item){
      if (!!err) next(new Error(err))
      else if (!item) winstonConf.handleErrorCode(400,'Invalid docid',next)
      else{
        var newwords = item.metadata.newwords
        var updated = false
        for (var i = 0; i < newwords.length; i++) {
            var del = false
            if ((newwords[i].line==req.query.line) && (newwords[i].fword<=req.query.fword) && (newwords[i].lword>=req.query.fword)) del = true
            else if ((newwords[i].line==req.query.line) && (newwords[i].fword>=req.query.fword) && (newwords[i].fword<=req.query.lword)) del = true
            if (del) {
              item.metadata.newwords.splice(i,1)
              updated = true
            }
        };
        if (!updated) winstonConf.handleErrorCode(400,'Invalid new word',next)
        else docs.update({'_id':BSON.ObjectID(req.params.docid)},
          {$set:{'metadata.newwords':item.metadata.newwords}},function(err,WriteResult){
            if (!!err) next(new Error(err))
            else if (WriteResult==0) next(new Error('Can\'t Remove Word'))
            else res.send()
          })
      }
    });


  }
  else winstonConf.handleErrorCode(401,'Permission Denied',next)
}