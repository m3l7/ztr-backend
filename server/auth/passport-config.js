var passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy
  , BearerStrategy = require('passport-http-bearer').Strategy
  , dbmod = require('../db')
  , ObjectID = require('mongodb').ObjectID
  , bcrypt = require('bcrypt')
  , conf = require('../conf.js')
  
  
passport.use(new LocalStrategy(
  function(username, password, done) { //db.connect(function(err,db){
    var db = dbmod.db
    var users = db.collection('users')

    //compute the hash of the password
    users.findOne({'username':username, activated: true}, function(err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      else{
        bcrypt.compare(password, user.password, function(err, resp) {
          if (!!err) return done(err)
          else if (!resp) return done(null,false)
          else return done(null, user);
        })
      }
    })
  }
));
  
  
  
passport.serializeUser(function(user, done) { 
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  var db = dbmod.db
  var users = db.collection('users')
  users.findOne({'_id':ObjectID(id)}, function (err, user) {
    done(err, user);
  });
});


  
passport.use(new BearerStrategy(
  function(accessToken, done) {
    var db = dbmod.db
    var tokens = db.collection('tokens')
    tokens.findOne({'token':accessToken}, function(err, token) {
      if (err) { return done(err); }
      if (!token) { return done(null, false); }
      
      var users = db.collection('users')
      users.findOne(token.user_id, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        
        //we don't want to store the user's password
        delete user.password
        
        var info = { scope: '*' }
        done(null, user, info);
      });
    });
  }
));