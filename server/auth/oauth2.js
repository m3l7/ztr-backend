var oauth2orize = require('oauth2orize')
  , passport = require('passport')
  , login = require('connect-ensure-login')
  , db = require('../db')
  , utils = require('./utils')
  , ObjectID = require('mongodb').ObjectID
  , server = oauth2orize.createServer()
  , winstonConf = require('../errorHandling/winstonConf.js')()
  , dbmod = require('../db')


    
    
server.serializeClient(function(client, done) {
  return done(null, client);
});

server.deserializeClient(function(id, done) {
    return done(null, id);
});
    
server.grant(oauth2orize.grant.code(function(client, redirectURI, user, ares, done) {
  var code = utils.uid(16)
  done(null, code);
}));
    
exports.decision = [
  server.decision()
]
    
    
exports.authorization = [
    server.authorization(function(clientID, redirectURI, done) {
      return done(null, clientID, redirectURI)
    }),
    function(req, res){
      for (property in req) {
      output += property + ': ' + req[property]+'; ';
    }
    res.send('dialog');
    }
]


exports.login = [
    function(req,res,next){req.logout();next();}
    ,passport.authenticate('local')
    ,function(req,res){
	     res.send();
    }
]

exports.logout = function(req, res) {
  req.logout();
  res.send()
}


exports.token = [
  function(req,res,next){
    next()
  },
  //token endpoint -- send a token if the user is logged in
  login.ensureLoggedIn(),
  function(req,res,next){
    var db = dbmod.db
    var code = utils.uid(16)
    var tokens = db.collection('tokens')
      var d = new Date()
      d.setMonth(d.getMonth() + 1);
      tokens.insert({user_id:ObjectID(req.user._id.id),token:code,expiry_date:d}
      ,function(err,token){
          if (!!err) next (new Error(err))
          else res.send(JSON.stringify({token: code,uid:req.user._id}))
      })
  }
]
