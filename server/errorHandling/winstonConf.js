var winston = require('winston')
, expressWinston = require('express-winston')
, conf = require('../conf.js')

var exp = {
	transports: [],
	winston: null,
	winston_noConsole: null,
	expressWinston: expressWinston,
	handleErrorCode: function(code,message,next){
		var err = new Error(message)
		err.status = code
		next(err)
	},
	errorMiddleware: function(err,req,res,next){
		if (!err.stack){
			res.send(500,err)
			exp.winston.error(err)	
		} 
		else{
			if ((!err.status) || (err.status==500)){
				res.send(500,err.stack)
				exp.winston.error(err.stack)	
			}
			else{
				res.send(err.status,err.message)
				exp.winston.warn(err.message)	
			}
		}

	}
}

module.exports = function(app){

		if (!!app){

			exp.winston_noConsole = new (winston.Logger)()
			exp.winston = new (winston.Logger)()

			if (conf.debug.console.enabled) {
				exp.winston.add(winston.transports.Console,conf.debug.console.options)
				exp.transports.push(new winston.transports.Console(conf.debug.console.options))
			}
			if (conf.debug.file.enabled) {
				exp.winston.add(winston.transports.File, conf.debug.file.options)
				exp.winston_noConsole.add(winston.transports.File, conf.debug.file.options)
				exp.transports.push(new winston.transports.File(conf.debug.file.options))
			}
		}

		return exp
}